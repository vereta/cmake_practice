#include "libstarwars.h"

#include <iostream>

namespace hometask {
    void StarWars::darth_vader() {
        std::cout << "Luke, I'm your father!" << std::endl;
    }
}