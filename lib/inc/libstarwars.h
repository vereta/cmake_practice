#pragma once

namespace hometask {
class StarWars
{
public:
    void chewbacca();
    void darth_vader();
    void luke_skywalker();
};
} // hometask