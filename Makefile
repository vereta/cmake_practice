NAME = app
CC = g++
CXXFLAGS = -Wall -Wextra -Werror

SRC_FLD = src
INC_FLD = inc
OBJ_FLD = obj

LIB_PATH = lib
LIB_LINK = -L $(LIB_PATH) -lstarwars

OBJECTS =\
	$(OBJ_FLD)/main.o

INCLUDES =\
	$(INC_FLD)/$(NAME).h

all : $(NAME)

$(NAME) : $(OBJECTS) $(INCLUDES)
	@make -C $(LIB_PATH)
	@$(CC) -I $(INC_FLD) $(CXXFLAGS) $(OBJECTS) -o $(NAME) $(LIB_LINK)
	@echo "\033[32m $(NAME) has been created \033[0m"

clean :
	@make clean -C $(LIB_PATH)
	@rm -rf $(OBJ_FLD)
	@echo "\033[33m Cleaning .o files of $(NAME) \033[0m"

fclean : clean
	@make fclean -C $(LIB_PATH)
	@rm -f $(NAME)
	@echo "\033[31m Binary $(NAME) has been deleted \033[0m"

re : fclean all

$(OBJ_FLD)/main.o : $(SRC_FLD)/main.cpp $(HEADERS)
	@mkdir -p $(OBJ_FLD)
	@$(CC) -I $(INC_FLD) -I $(LIB_PATH)/$(INC_FLD) $(CXXFLAGS) -c $(SRC_FLD)/main.cpp -o $(OBJ_FLD)/main.o
