#include "app.h"

int main()
{
    hometask::StarWars star_wars;

    star_wars.darth_vader();
    star_wars.luke_skywalker();
    star_wars.chewbacca();

    return 0;
}